<?php
//CREAMOS LA CLASE LLAMADA USUARIO
class usuario{
//damos los atributos de la clase usuario
   public $nombre;
   public $correo;
   private $contraseña;
   protected $fecha_registro;
//ponemos nuestro contructor con parametros
 public function __construct($nombre, $correo ){
     $this->nombre=$nombre;
     $this->correo=$correo;
     // AQUI USAMOS EL COMANDO RAND PARA GENERAR UNA CONTRASEÑA DIFERENTE CADA VEZ QUE SE REPITA EL CODIGO DE CONTRASEÑA
     $this->contraseña= rand();
     //AQUI AGREGAMOS UN COMANDO LLAMADO DATE PARA PONER LA FECHA EXACTA DEL REGISTRO
     $this->fecha_registro = date('Y-m-d H:m:s');
}

//METODOS DE LA CLASE USUARIO
    public function perfil(){
        echo "Datos del usuario <br>";
        echo "Nombre: ". $this->nombre."<br>";
        echo "Email: ". $this->correo."<br>";
        echo "Contraseña: ". $this->contraseña."<br>";
        echo "Fecha de registro: ". $this->fecha_registro."<br>";

    }
//LOS ATRIBUTOS CON LA MODIFICACION DE ACCESO "PRIVATE" SE PUEDEN LLAMAR MIENTRAS ESTEN EN LA MISMA CLASE, SI SE PONEN EN DIFERENTES
//CLASSES O FUERA DE LA QUE SE LES MENCIONA ESTA NO SE MOSTRARA EN LA PANTALLA POR MAS QUE LO MANDEN A LLAMAR
//LAS DECLARACIONES DE TIPO PROTECTED TAMPOCO SE PUEDEN MOSTRAR EN PANTALLA FUERA DE LA CLASE, TIENE QUE SER LLAMADA DESDE LA CLASE
//EN LA QUE SE LE DECLARO, ESTA MISMA PUEDE SER LLAMADA CON LA HERENCIA YA QUE SON PARTE DE LA CLASE Y TIENEN PERMISO DE MODIFICAR
//LOS MODIFICADORES DE ACCESO PUBLIC PUEDEN SER LLAMADOS DE CUALQUIER PARTE DEL CODIGO, SIEMPRE Y CUANDO CONTENGA ALGO PARA MOSTRAR    


 }
?>