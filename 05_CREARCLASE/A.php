<?php
class Horoscopo{
    



 public $Aries = "Demoras y frustraciones te agobiarán todo el día, especialmente en el terreno de las comunicaciones. La visita que esperabas hoy en casa podría retrasarse y quizás no pueda comunicarte su intención. No vale la pena que te preocupes. Llegado el momento, tu visitante llegará sano y salvo. Su demora puede deberse al tránsito u otros imprevistos. No te desanimes.";
 public $Tauro = "Tendrás a tu disposición un nuevo equipo que te resultará fascinante, y desearás aprender todo lo que puedas acerca de su funcionamiento. Por lo tanto, es un gran día para aumentar tus conocimientos de computación. También querrás probar tus habilidades en la grabación de audio y vídeo. El arte combinado con la moderna tecnología será el enfoque más importante del día. Aprende lo más que puedas y trata de divertirte en el camino";
 public $Geminis = "Hoy puede ocurrir que debas asumir el papel de pacificador. La gente aprecia tu sentido de la justicia en tiempos de crisis y confía en tu discernimiento. Te has ganado su respeto, por lo que esta vez puede no ser la última. Debido a la disposición planetaria de este día, te resultará fácil escuchar con atención a ambas partes y evaluar tus decisiones desde la inteligencia y la integridad emocional, dejando de lado tus afinidades personales.";
 public $Cancer = "Un sueño o presentimiento intenso resultará un avance espiritual de algún tipo, y pasarás gran parte del día un poco aturdida, tratando de encontrarle sentido a la situación. Busca libros sobre el tema, ya que la lectura te brindará mucha información útil. También podrías escribir una reseña de la experiencia";
 public $Leo = "La actividad del día posiblemente sea dinámica, por lo tanto asegúrate de tener el cinturón de seguridad ajustado. ¿Qué gracia tendría la vida si no hubiera cada tanto algunos tumbos en el camino para que las cosas sean vivaces e interesantes? Los eventos del día de hoy probablemente se pasen de la raya y generen un jaleo con tus emociones. Descubrirás que mientras te esfuerzas en mantener todo funcionando, de repente aparece un obstáculo en tu camino. No dejes que esto te haga perder la visión de tus logros";
 public $Virgo = "Hoy puede resultarte sencillo mantener todo bajo control. Toma tu planificador diario o tu agenda personal y actualízalos. Quizás no seas el tipo de persona que no puede despegarse del calendario ni por un minuto, pero no te costará ningún esfuerzo mantener a raya esa lista de tareas que está adherida en la heladera. Éste es un buen día para fijarte algunas metas personales, porque te resultará muy sencillo saber exactamente lo que tienes que hacer para conseguirlas.";
 public $Libra = "Por razones fuera de tu control -tráfico, construcción o ambos- hoy te será imposible hacer esos viajecitos por tu barrio. Si tienes que hacer mandados, ni te molestes. Otra alternativa es que los hagas a pie o en bicicleta. Si debes conducir, asegúrate de llevar tu música preferida para entretenerte mientras esperas en tráfico. Si no te morirás de aburrimiento";
 public $Escorpio = "Hoy te espera una pila de documentos tediosos. Probablemente, exigirá toda tu atención. De ser factible, ocúpate de eso a primera hora cuando estás con la mente fresca. Podría tratarse de un contrato, póliza de seguro u otro documento lleno de lenguaje incomprensible.";
 public $Sagitario = "¡Hoy tienes un gran día por delante! Tu capacidad de decisión se encontrará en su mejor momento, lo que te permitirá resolver cualquier asunto importante con mayor rapidez que de costumbre, ya que los aspectos esenciales te resultarán muy evidentes. Además, tu energía física te provocará placer. ¿Qué más puede pedir una persona? Sonríe. Hoy no habrá nada que no puedas manejar";
 public $Capricornio = "Los estudios espirituales ocuparán gran parte de tu energía el día de hoy. Tu grupo de estudio te cargará con un montón de obligaciones por cumplir, o acontecimientos a los que debas asistir, o demasiada información para procesar. O todo lo antes mencionado. Así no aprenderás nada";
 public $Acuario = "Aunque pareciera que los próximos meses se perfilan para que sea un gran momento profesional para ti, hoy todo parecerá imposible. Tareas mundanas, rutinarias y no gratificantes absorberán todo tu tiempo. No obstante, no creas que esto te lo llevarás a tu tumba, no dejes que tu frustración interfiera con tus perspectivas para el futuro";
 public $piscis = "Cuidado con querer tener demasiada información! El cerebro tiene una capacidad limitada para tratar y asimilar lo que lo damos, no quieras bailar más rápido que la música. Sientes que tienes que atrapar el tiempo perdido, pero así sólo conseguirás cansarte y no asimilarás nada. Se paciente y haz las cosas poco a poco. Serás más efectiva de esta manera";

 public function setAries($MD_Abril){
   $this->aries = $MD_Abril;
 }

 public function getAries(){
   return $this->aries;
 }

 public function setTauro($MD_Abril){
   $this->tauro = $MD_Abril;
 }

 public function getTauro(){
   return $this->tauro;
 }

 public function setGeminis($MD_Abril){
   $this->geminis = $MD_Abril;
 }

 public function getGeminis(){
   return $this->geminis;
 }

 public function setCancer($MD_Abril){
   $this->cancer = $MD_Abril;
 }

 public function getCancer(){
   return $this->cancer;
 }

 public function setLeo($MD_Abril){
   $this->leo = $MD_Abril;
 }

 public function getLeo(){
   return $this->leo;
 }

 public function setVirgo($MD_Abril){
   $this->virgo = $MD_Abril;
 }

 public function getVirgo(){
   return $this->virgo;
 }    

 public function setLibra($MD_Abril){
   $this->libra = $MD_Abril;
 }

 public function getLibra(){
   return $this->libra;
 }

 public function setEscorpio($MD_Abril){
   $this->escorpio = $MD_Abril;
 }

 public function getEscorpio(){
   return $this->escorpio;
 }

 public function setSagitario($MD_Abril){
   $this->sagitario = $MD_Abril;
 }

 public function getSagitario(){
   return $this->sagitario;
 }

 public function setCapricornio($MD_Abril){
   $this->capricornio = $MD_Abril;
 }

 public function getCapricornio(){
  return $this->capricornio;
 }

 public function setAcuario($MD_Abril){
   $this->acuario = $MD_Abril;
 }

 public function getAcuario(){
  return $this->acuario;
 }

 public function setPiscis($MD_Abril){
   $this->piscis = $MD_Abril;
 }

 public function getPiscis(){
   return $this->piscis;
 }

} 

?>

